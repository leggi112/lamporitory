import tkinter as tk
from lamp import Lamp
import colorsys


class LampFrame(tk.Frame):

    def __init__(self, master, lamp, *args, width=300, height=50, **kwargs):
        self.name = ""
        self.state = {}
        self.color = ""
        self.master = master
        self.lamp = lamp
        self.font_color = "#000"
        self.process_lamp_data()

        super().__init__(master, *args, width=width, height=height, bg=self.color, **kwargs)

        self.name_label = tk.Label(self, text=self.name, font="Calibri 13 bold", fg=self.font_color, bg=self.color)
        self.name_label.place(relx=0.1, rely=0.5, anchor="w")

    def process_lamp_data(self):
        self.name = self.lamp["name"]
        self.state = self.lamp["state"]
        color = (self.state["hue"]/65535, self.state["sat"]/255, self.state["bri"]/255)
        if color[2] < 0.4:
            self.font_color="#FFF"
        r, g, b = colorsys.hsv_to_rgb(*color)
        r = int(r * 255)
        g = int(g * 255)
        b = int(b * 255)
        hexstr_r=str(hex(r))[2:]
        if len(hexstr_r)<2:
            hexstr_r = "0"+hexstr_r

        hexstr_g=str(hex(g))[2:]
        if len(hexstr_g)<2:
            hexstr_g = "0"+hexstr_g

        hexstr_b=str(hex(b))[2:]
        if len(hexstr_b)<2:
            hexstr_b = "0"+hexstr_b

        self.color = f"#{hexstr_r}{hexstr_g}{hexstr_b}"
        if not self.state["on"]:
            self.color = "#000"
