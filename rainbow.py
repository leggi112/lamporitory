# from lamporitory import Hue
import time
import threading as th


def run_rainbow(lamp):
    r = 36
    for i in range(r):
        lamp.set_color_hsv((i/r), 1, 1)
        time.sleep(0.75)
