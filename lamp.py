import requests
import colorsys


class Lamp:

    def __init__(self, dic: dict, url: str, number: int):
        self.properties = dic
        self.number=number
        self.url = url + f"/{number}"

    def set(self,status: bool):
        data = {"on": status}
        requests.put(self.url + "/state", json=data)
        self.properties["state"]["on"] = status

    def set_off(self):
        data = {"on": False}
        requests.put(self.url + "/state", json=data)
        self.properties["state"]["on"] = False

    def set_on(self):
        data = {"on": True}
        requests.put(self.url + "/state", json=data)
        self.properties["state"]["on"] = True

    def set_color_rgb(self, r: int, g: int, b: int):
        if r < 0 or r >= 256:
            raise ValueError
        elif g < 0 or r >= 256:
            raise ValueError
        elif b < 0 or r >= 256:
            raise ValueError

        r = r/255
        g = g/255
        b = b/255

        h, s, v = colorsys.rgb_to_hsv(r, g, b)
        data={"on": True, "sat": int(s*255), "bri": int(v*255), "hue": int(h*65535)}
        requests.put(self.url + "/state", json=data)
        self.properties["state"].update(data)

    def set_color_rgb_normalized(self, r: float, g: float, b: float):
        if r < 0.0 or r > 1.0:
            raise ValueError
        elif g < 0.0 or r > 1.0:
            raise ValueError
        elif b < 0.0 or r > 1.0:
            raise ValueError

        h, s, v = colorsys.rgb_to_hsv(r, g, b)
        data = {"on": True, "sat": int(s * 255), "bri": int(v * 255), "hue": int(h*65535)}
        requests.put(self.url + "/state", json=data)
        self.properties["state"].update(data)

    def set_color_hsv(self,h: float, s: float, v: float):
        if h < 0.0 or h > 1.0:
            raise ValueError
        elif s < 0.0 or s > 1.0:
            raise ValueError
        elif v < 0.0 or v > 1.0:
            raise ValueError

        data = {"on": True, "sat": int(s * 255), "bri": int(v * 255), "hue": int(h * 65535)}
        requests.put(self.url + "/state", json=data)
        self.properties["state"].update(data)

    def __getitem__(self, item: str):
        return self.properties[item]
