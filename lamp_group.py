

class LampGroup:

    def __init__(self, data: dict, number: int, lamps: dict):
        self.data = data
        self.number = number
        self.lamps = self.process_input_data(lamps)

    def process_input_data(self, lamps):
        result=[]
        for key in self.data["lights"]:
            result.append(lamps[key])
        return result

    def set_color_rgb(self, r: int, g: int, b: int):
        if r < 0 or r >= 256:
            raise ValueError
        elif g < 0 or r >= 256:
            raise ValueError
        elif b < 0 or r >= 256:
            raise ValueError

        for lamp in self.lamps:
            lamp.set_color_rgb(r, g, b)

    def set_color_rgb_normalized(self, r: float, g: float, b: float):
        if r < 0.0 or r > 1.0:
            raise ValueError
        elif g < 0.0 or r > 1.0:
            raise ValueError
        elif b < 0.0 or r > 1.0:
            raise ValueError

        for lamp in self.lamps:
            lamp.set_color_rgb_normalized(r, g, b)

    def set_color_hsv(self,h: float, s: float, v: float):
        if h < 0.0 or h > 1.0:
            raise ValueError
        elif s < 0.0 or s > 1.0:
            raise ValueError
        elif v < 0.0 or v > 1.0:
            raise ValueError

        for lamp in self.lamps:
            lamp.set_color_hsv(h, s, v)
