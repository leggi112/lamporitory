import requests
import json
import time

import rainbow
from lamp import Lamp
from lamp_group import LampGroup


class Hue:

    def __init__(self, user: str, ip: str):
        self.user = user
        self.ip = ip

        self.url = f"http://{self.ip}/api/{self.user}"
        self.lamp_dic = self.get_lamp_data()
        self.lamps = self.init_lamps()
        self.groups = self.init_groups()

    def get_lamp_data(self):
        result = requests.get(self.url+"/lights")
        result = json.loads(result.text)
        return result

    def init_lamps(self):
        result = {}
        for key in self.lamp_dic:
            result[key] = Lamp(self.lamp_dic[key], self.url+"/lights", int(key))
        return result

    def init_groups(self):
        data = requests.get(self.url+"/groups")
        data = json.loads(data.text)
        result={}

        for key in data:
            group = LampGroup(data[key], int(key), self.lamps)
            result[key] = group
        return result

    def get_lamp(self, key: int or str):
        return self.lamps(str(key))


if __name__ == "__main__":
    hue = Hue('ucpzuPgnyGzrJ6pqEJynNg8ZvBIauIRQXTYWFA21', '192.168.178.20')
    print(hue.url)
    # hue.groups["1"].set_color_rgb(0,255,0)
    hue.lamps["9"].set_color_rgb_normalized(1,1,1)
