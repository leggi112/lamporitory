import tkinter as tk
from lamporitory import Hue
from lamp_frame import LampFrame


class LampListFrame(tk.Frame):

    def __init__(self, master, lamps: dict, *args, width=300, height=300, **kwargs):
        self.master = master
        self.lamps = lamps
        super().__init__(master, *args, width=width, height=height, **kwargs)
        self.create_lamp_frames()

    def create_lamp_frames(self):
        result = []
        i = 0
        for lamp in self.lamps.values():
            lamp_frame = LampFrame(self, lamp)
            lamp_frame.grid(row=i, column=0,pady=1)
            result.append(lamp_frame)
            i += 1


if __name__ == "__main__":
    root = tk.Tk()
    hue = Hue('ucpzuPgnyGzrJ6pqEJynNg8ZvBIauIRQXTYWFA21', '192.168.178.20')
    main = LampListFrame(root, hue.lamps)
    main.pack()
    root.mainloop()
